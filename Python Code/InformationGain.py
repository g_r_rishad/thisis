import pandas as pd
# from __future__ import division
from math import log
import numpy as np


def entropy(pi):
    total = 0
    for p in pi:
        p = p / sum(pi)
        if p != 0:
            total += p * log(p, 2)
        else:
            total += 0
    total *= -1
    return total


def gain(d, a):

    total = 0
    for v in a:
        total += sum(v) / sum(d) * entropy(v)

    gain = entropy(d) - total
    return gain


###__ example 2 (playTennis homework)
# set of example of the dataset
playTennis = [9, 5]  # Yes, No

# attribute, number of members (feature)
outlook = [
    [4, 0],  # overcase
    [2, 3],  # sunny
    [3, 2]   # rain
]
temperature = [
    [2, 2],  # hot
    [3, 1],  # cool
    [4, 2]   # mild
]
humidity = [
    [3, 4],  # high
    [6, 1]   # normal
]
wind = [
    [6, 2],  # weak
    [3, 3]   # strong
]

# print(gain(playTennis, outlook))
# print(gain(playTennis, temperature))
# print(gain(playTennis, humidity))
# print(gain(playTennis, wind))


def calculate_attribute_table(df, key, target_key, target):
    newDf = df[[key, target_key]]
    attr_table = []
    for value in np.unique(newDf[[key]]):
        atrr_value = []
        for target_value in np.unique(newDf[[target_key]]):
            val = newDf[(newDf[key] == value) & (
                newDf[target_key] == target_value)][[key]].count()
            atrr_value.append(val.iloc[0].astype(np.int))
        attr_table.append(atrr_value)

    return attr_table


def view_info_gain(df, info_df):
    # target_col = 'species'
    target_col = 'dwmhcmp'
    targetDf = df[target_col].value_counts()
    target = np.unique(targetDf.astype(np.int))
    index = 1
    for key in df.keys()[:-1]:
        each = calculate_attribute_table(
            df, key, target_col, target)
        gain_value = gain(target, each)
        # print('[{0}] {1} infomration gain is = {2}'.format(
        #     str(index), key, str(gain_value)))
        info_df = append_info_gain(info_df,key,gain_value)
        index = index + 1

    return info_df

def create_info_dataframe():
    col_names =  ['Attribute', 'InformationGain']
    info_df  = pd.DataFrame(columns = col_names)

    return info_df

def append_info_gain(df,col_name,gain):
    df.loc[len(df)] = [col_name, str(gain)]

    return df

def save_info_gain(info_df, fileName):
    info_df.to_csv(fileName,sep = ',', encoding = 'utf-8')


df = pd.read_csv('data/dataset.csv')
# df = pd.read_csv('data/test.csv')
df.head()

info_gain = create_info_dataframe()

info_gain = view_info_gain(df,info_gain)

fileName = 'data/info_gain.csv'

save_info_gain(info_gain, fileName)

print('Done')
