import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.interpolate import spline
from sklearn.cluster import KMeans
from kmodes.kmodes import KModes

def read_dataset(path):
    data = pd.read_csv(path)
    return data

def build_kmeans_graph(df):
    kmeans = KModes(n_clusters=4, init='Huang', n_init=5, verbose=1)
    y_kmeans = kmeans.fit_predict(df)
    return y_kmeans, kmeans

def kmeans_graph(df, y_kmeans, kmeans):
    plt.scatter(df[y_kmeans == 0, 0], df[y_kmeans == 0, 1], s = 100, c = 'red', label = 'Cluster 1')
    # plt.scatter(df[y_kmeans == 1, 0], df[y_kmeans == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
    # plt.scatter(df[y_kmeans == 2, 0], df[y_kmeans == 2, 1], s = 100, c = 'green', label = 'Cluster 3')
    # plt.scatter(df[y_kmeans == 3, 0], df[y_kmeans == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
    #plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
    plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 300, c = 'yellow', label = 'Centroids')

    plt.legend()
    plt.show()

def main():
    file_name = 'data/dataset.csv'
    df = read_dataset(file_name)
    y_means,kmeans = build_kmeans_graph(df)
    kmeans_graph(df, y_means, kmeans)

main()