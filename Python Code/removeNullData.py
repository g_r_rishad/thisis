import pandas as pd


df = pd.read_csv('data/raw-data.csv')
df.head()
# df = df.fillna('NULL')
df = df.dropna(how='any', axis = 0)

# print(df.isnull().sum())

df.to_csv('data/after_remove_null_row.csv',sep = ',', encoding = 'utf-8')
print('done')

