import pandas as pd
from sklearn import preprocessing
import numpy as np
import csv

def LabelEncoding(df):
    print('Total Column' + str(df.shape[1]))
    for x in range(0, df.shape[1]):
        # encode = Encoding(df.iloc[:,x])
        df.iloc[:,x] = Encoding(df.iloc[:,x], df.columns[x])
    
    return df


def Encoding(object, column_name):
    le = None
    le = preprocessing.LabelEncoder()
    le.fit(object.astype(str))
    WriteEncodingEvidence(le.classes_ , column_name)
    return le.transform(object.astype(str))

def WriteEncodingEvidence(labelEncoding, column_name) :
        # file = open('data/label_encoding_evidence.txt','a')
        # file.write('\n')
        # for i,item in enumerate(labelEncoding):
        #     file.write(item +'  =>  ' + str(i) + '\n')
    with open('data/label_encoding_evidence.csv', 'a') as csvfile:
        fieldnames = ['Label', 'Encodeing Value']
        writer = csv.DictWriter(csvfile, delimiter=',',fieldnames=fieldnames)
        writer.writerow({'Label': column_name, 'Encodeing Value': column_name})
        for i,item in enumerate(labelEncoding):
            writer.writerow({'Label': item, 'Encodeing Value': str(i)})


df = pd.read_csv('data/raw-data.csv')
df.head()

df = LabelEncoding(df)
df.to_csv('data/after_encoded_column.csv',sep = ',', encoding = 'utf-8')

