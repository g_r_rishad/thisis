# K-Means Clustering

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.interpolate import spline 
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d
# Importing the dataset
dataset = pd.read_csv('data/dataset.csv')
# X = dataset.iloc[:, [3, 4]].values
X = dataset.iloc[:, 0:len(dataset)].values

# y = dataset.iloc[:, 3].values

# Splitting the dataset into the Training set and Test set
"""from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)"""

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""

# Using the elbow method to find the optimal number of clusters
from sklearn.cluster import KMeans
# wcss = []
# for i in range(1, 11):
#     kmeans = KMeans(n_clusters = i, init = 'k-means++', random_state = 42)
#     kmeans.fit(X)
#     wcss.append(kmeans.inertia_)
# plt.plot(range(1, 11), wcss)
# plt.title('The Elbow Method')
# plt.xlabel('Number of clusters')
# plt.ylabel('WCSS')
# plt.show()

"""Graph Smoothing"""

# x = np.array(range(1,11))
# y = np.array(wcss)

# x_smooth = np.linspace(x.min(), x.max(), 300)
# y_smooth = spline(x,y, x_smooth)

# plt.plot(x_smooth, y_smooth)
# plt.title('Graph Smoothing')
# plt.xlabel('Number of clusters')
# plt.ylabel('WCSS')
# plt.show()


ax = plt.axes(projection='3d')



# Fitting K-Means to the dataset
kmeans = KMeans(n_clusters = 3, init = 'k-means++', random_state = 42)
y_kmeans = kmeans.fit_predict(X)


# Visualising the clusters
# plt.scatter(X[y_kmeans == 0, 43], X[y_kmeans == 0, 37], s = 100, c = 'red', label = 'Cluster 1')
# # plt.scatter(X[y_kmeans == 1, -1], X[y_kmeans == 1, 37], s = 100, c = 'blue', label = 'Cluster 2')
# # plt.scatter(X[y_kmeans == 2, -1], X[y_kmeans == 2, 37], s = 100, c = 'green', label = 'Cluster 3')
# #plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
# #plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'magenta', label = 'Cluster 5')
# plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 300, c = 'yellow', label = 'Centroids')

# ax.plot3D(xline, yline, zline, 'gray')

zdata = X[y_kmeans == 0, 57]
xdata = X[y_kmeans == 0, 55]
ydata = X[y_kmeans == 0, 49]
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens', label = 'Cluster 1');

zdata = X[y_kmeans == 1, 57]
xdata = X[y_kmeans == 1, 55]
ydata = X[y_kmeans == 1, 49]
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Oranges',label = 'Cluster 2');

zdata = X[y_kmeans == 2, 57]
xdata = X[y_kmeans == 2, 55]
ydata = X[y_kmeans == 2, 49]
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Blues',label = 'Cluster 3');

# zdata = X[y_kmeans == 3, 43]
# xdata = X[y_kmeans == 3, 37]
# ydata = X[y_kmeans == 3, 41]
# ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='winter',label = 'Cluster 4');

ax.set_xlabel('Work Position')
# ax.set_ylabel('Work Position')
ax.set_ylabel('Age')
ax.set_zlabel('Medical Professional Decision');

plt.legend()
plt.show()
