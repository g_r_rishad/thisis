import pandas as pd
import numpy as np

def read_dataframe():
    df = pd.read_csv('data/after_encoded_column.csv')
    df.head()
    return df

def remove_unique_coulmn(df, percentage):
    newdf = df
    print(df.shape[1])
    for col in df.columns:
        numberOfUniqueValue = df[col].nunique()
        # print('{0} = {1}'.format(str(col), numberOfUniqueValue))
        totalCount =  df[col].count()
        if(((numberOfUniqueValue / totalCount) * 100) >= percentage):
            newdf.drop(str(col), axis=1, inplace=True)
            print(((numberOfUniqueValue / totalCount) * 100))
    
    print(newdf.shape[1])
    return newdf


df = read_dataframe()

newdf = remove_unique_coulmn(df, 40)

newdf.to_csv('data/after_drawback_ig_remove.csv',sep = ',', encoding = 'utf-8')







