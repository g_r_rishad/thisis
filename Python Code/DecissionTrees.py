import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import time

INPUT_PATH = "data/dataset.csv"

def Load_dataset(path):
    data = pd.read_csv(path)
    return data

def get_features(dataset):
    features = []
    for feature in dataset.keys()[:-1]:
        features.append(feature)
    
    return features

def decision_tree_classifier(features, target):
    """
    To train the random forest classifier with features and target data
    :param features:
    :param target:
    :return: trained random forest classifier
    """
    clf = DecisionTreeClassifier()
    clf.fit(features, target)
    return clf

def split_dataset(dataset, train_percentage, feature_headers, target_header):
    # Split dataset into train and test dataset
    train_x, test_x, train_y, test_y = train_test_split(dataset[feature_headers], dataset[target_header],
                                                        train_size=train_percentage)
    return train_x, test_x, train_y, test_y


def main():
    dataset = pd.read_csv(INPUT_PATH)


    target_header = 'dwmhcmp'
    HEADERS = get_features(dataset)

    # dataset = handel_missing_values(dataset, HEADERS[6], '?')
    train_x, test_x, train_y, test_y = split_dataset(dataset, 0.33, HEADERS, target_header)

    # Train and Test dataset size details
    print("Train_x Shape :: ", train_x.shape)
    print("Train_y Shape :: ", train_y.shape)
    print("Test_x Shape :: ", test_x.shape)
    print("Test_y Shape :: ", test_y.shape)

    # Create random forest classifier instance
    trained_model = decision_tree_classifier(train_x, train_y)
    print("Trained model :: ", trained_model)
    predictions = trained_model.predict(test_x)

    # print(sorted(test_y))
    # length = len(HEADERS) -1
    for i in range(0, 10):
        print("Actual outcome :: {0} and Predicted outcome :: {1}".format(list(test_y)[i], predictions[i]))
    print("Train Accuracy :: ", accuracy_score(train_y, trained_model.predict(train_x)))
    print("Test Accuracy  :: ", accuracy_score(test_y, predictions))
    print(" Confusion matrix ", confusion_matrix(test_y, predictions))

main()
print(time.process_time())

