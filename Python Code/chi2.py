import pandas as pd
import numpy as np
import scipy.stats as stats
from scipy.stats import chi2_contingency



class ChiSquare:
    def __init__(self, dataframe):
        self.df = dataframe
        self.p = None #P-Value
        self.chi2 = None #Chi Test Statistic
        self.dof = None
        
        self.dfObserved = None
        self.dfExpected = None

    def _print_chisquare_result(self, colX, alpha,chi_df):
        result = ""
        if self.p<alpha:
            result="{0} is IMPORTANT for Prediction, chi-square value is : {1}, dof : {2}".format(colX,self.chi2, self.dof)
            chi_df = append_chiSquare_value(chi_df,colX,str(self.chi2),self.p, result = 'Important')
        else:
            result="{0} is NOT an important predictor. (Discard {0} from model) {1}".format(colX, self.dof)
            chi_df = append_chiSquare_value(chi_df,colX,str(self.chi2),self.p, result ='Not Important')

        print(result)
        return chi_df
    
    def TestIndependence(self,colX,colY, chi_df,alpha=0.05):
        X = self.df[colX].astype(str)
        Y = self.df[colY].astype(str)
        
        self.dfObserved = pd.crosstab(Y,X) 
        chi2, p, dof, expected = stats.chi2_contingency(self.dfObserved.values)
        self.p = p
        self.chi2 = chi2
        self.dof = dof 
        
        self.dfExpected = pd.DataFrame(expected, columns=self.dfObserved.columns, index = self.dfObserved.index)
        
        return self._print_chisquare_result(colX,alpha,chi_df)

def create_chiSquare_dataframe():
    col_names =  ['Attribute', 'Chi_square_value', 'P_value', 'Result']
    info_df  = pd.DataFrame(columns = col_names)

    return info_df

def append_chiSquare_value(df,col_name,chi_value, p_value, result):
    df.loc[len(df)] = [col_name, chi_value, p_value, result]

    return df

def save_chiSquare_table(info_df, fileName):
    info_df.to_csv(fileName,sep = ',', encoding = 'utf-8')


# df = pd.read_csv('data/test.csv')
df = pd.read_csv('data/dataset.csv')
# df['dummyCat'] = np.random.choice([0, 1], size=(len(df),), p=[0.5, 0.5])
df.head()

chi_square_df = create_chiSquare_dataframe()


fileName = 'data/chi_square.csv'



cT = ChiSquare(df)
#Feature Selection
for var in df.keys()[:-1]:
    # cT.TestIndependence(colX=var,colY="dwmhcmp" )
    chi_square_df = cT.TestIndependence(var,"dwmhcmp", chi_square_df)

save_chiSquare_table(chi_square_df, fileName)

print('Done')


