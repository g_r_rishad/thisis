import pandas as pd
import numpy as np
from random import randint

def InserRow(dataFrame, insertTotalRow) :
    rowCount = dataFrame.shape[0]
    for x in range(0, insertTotalRow):
        random = randint(0,rowCount)
        dataFrame.loc[len(dataFrame)] = dataFrame.iloc[random]
    
    dataFrame.to_csv('data/after_insert_row_new2.csv',sep = ',', encoding = 'utf-8')
    print('done')

df = pd.read_csv('data/after_remove_null_row.csv')
df.head()

InserRow(df,2000)

