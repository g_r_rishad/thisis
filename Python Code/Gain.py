import numpy as np
import pandas as pd
eps = np.finfo(float).eps
from numpy import log2 as log
import pprint


def find_entropy(df):
    # To make the code generic, changing target variable class name
    Class = df.keys()[-1]
    entropy = 0
    values = df[Class].unique()
    for value in values:
        fraction = df[Class].value_counts()[value]/len(df[Class])
        entropy += -fraction*np.log2(fraction)
    return entropy


def find_entropy_attribute(df, attribute):
  # To make the code generic, changing target variable class name
  Class = df.keys()[-1]
  target_variables = df[Class].unique()  # This gives all 'Yes' and 'No'
  # This gives different features in that attribute (like 'Hot','Cold' in Temperature)
  variables = df[attribute].unique()
  entropy2 = 0
  for variable in variables:
      entropy = 0
      for target_variable in target_variables:
          num = len(df[attribute][df[attribute] == variable]
                    [df[Class] == target_variable])
          den = len(df[attribute][df[attribute] == variable])
          fraction = num/(den+eps)
          entropy += -fraction*log(fraction+eps)
      fraction2 = den/len(df)
      entropy2 += -fraction2*entropy
  return abs(entropy2)


def find_winner(df):
    Entropy_att = []
    IG = []
    for key in df.keys()[:-1]:
        Entropy_att.append(find_entropy_attribute(df,key))
        IG.append(find_entropy(df)-find_entropy_attribute(df, key))
    return df.keys()[:-1][np.argmax(IG)], Entropy_att


def get_subtable(df, node, value):
  return df[df[node] == value].reset_index(drop=True)


def buildTree(df, tree=None):
    # To make the code generic, changing target variable class name
    Class = df.keys()[-1]

    #Here we build our decision tree

    #Get attribute with maximum information gain
    node, entropy = find_winner(df)

    for i in entropy:
        print(i)

    #Get distinct value of that attribute e.g Salary is node and Low,Med and High are values
    attValue = np.unique(df[node])

    #Create an empty dictionary to create tree
    if tree is None:
        tree = {}
        tree[node] = {}

   #We make loop to construct a tree by calling this function recursively.
    #In this we check if the subset is pure and stops if it is pure.

    for value in attValue:

        subtable = get_subtable(df, node, value)
        clValue, counts = np.unique(subtable['dwmhcmp'], return_counts=True)

        if len(counts) == 1:  # Checking purity of subset
            tree[node][value] = clValue[0]
        else:
            # Calling the function recursively
            tree[node][value] = buildTree(subtable)

    return tree

df = pd.read_csv('data/dataset.csv')
# df = pd.read_csv('data/test.csv')
df.head()

# dataset = {'Taste':['Salty','Spicy','Spicy','Spicy','Spicy','Sweet','Salty','Sweet','Spicy','Salty'],
#        'Temperature':['Hot','Hot','Hot','Cold','Hot','Cold','Cold','Hot','Cold','Hot'],
#        'Texture':['Soft','Soft','Hard','Hard','Hard','Soft','Soft','Soft','Soft','Hard'],
#        'Eat':['No','No','Yes','No','Yes','Yes','No','Yes','Yes','Yes']}

# df = pd.DataFrame(dataset,columns=['Taste','Temperature','Texture','Eat'])


tree = buildTree(df)
# file = open('data/tree.txt','a')
# file.write(str(tree))
pprint.pprint(tree)