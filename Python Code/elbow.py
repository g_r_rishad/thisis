import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.interpolate import spline
from sklearn.cluster import KMeans


def read_dataset(path):
    data = pd.read_csv(path)
    return data


def build_elbow_graph(df):
    wcss = []
    # X = df[:,-1].values
    for i in range(1, 11):
        kmeans = KMeans(n_clusters=i, init='k-means++', random_state=42)
        kmeans.fit(df.values)
        wcss.append(kmeans.inertia_)
    plt.plot(range(1, 11), wcss)
    plt.title('The Elbow Method')
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS')
    plt.show()
    return wcss

def graph_smothing(wcss):
    x = np.array(range(1, 11))
    y = np.array(wcss)

    x_smooth = np.linspace(x.min(), x.max(), 300)
    y_smooth = spline(x, y, x_smooth)

    plt.plot(x_smooth, y_smooth)
    plt.title('Graph Smoothing')
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS')
    plt.show()

def main():
    File_path = 'data/dataset.csv'
    df = read_dataset(File_path)
    wcss = build_elbow_graph(df)
    graph_smothing(wcss)

main()
